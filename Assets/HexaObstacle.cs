using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexaObstacle : MonoBehaviour
{
    public Rigidbody2D rb;

    public float shrinkRate = 3f;
    public float lastShrinkSize = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        rb.rotation = Random.Range(0f, 360f);
        transform.localScale = Vector3.one * Random.Range(10f, 12f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale -= Vector3.one* shrinkRate * Time.deltaTime;

        if (transform.localScale.x < lastShrinkSize)
        {
            Destroy(gameObject);
        }
    }
}

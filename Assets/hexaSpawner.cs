using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hexaSpawner : MonoBehaviour
{
    public float spawnRate = 1f;

    public GameObject Obstacle; //enter the obstacle prefab here;

    public float RateDelay = 0f;


    // Update is called once per frame
    void Update()
    {
        if(Time.time > RateDelay)
        {
            Instantiate(Obstacle,Vector3.zero, Quaternion.identity);
            RateDelay = Time.time + 1 / spawnRate;
        }
    }
}

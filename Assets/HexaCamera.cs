using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexaCamera : MonoBehaviour
{
    public GameObject player;
    public float lagSpeed = 2f;


    private Quaternion playerCurrentRotation;
    // Start is called before the first frame update
    void Start()
    {
        playerCurrentRotation = player.transform.rotation;   
    }

    // Update is called once per frame
    void Update()
    {
        playerCurrentRotation = player.transform.rotation;
        //transform.rotation = playerCurrentRotation; //
        transform.rotation = Quaternion.Lerp(transform.rotation, playerCurrentRotation, lagSpeed * Time.deltaTime);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class HexaPlayer : MonoBehaviour
{
    // Start is called before the first frame update

    public float speed = 600f;
    public float move = 0f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        move = Input.GetAxisRaw("Horizontal");

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
            {
                Vector2 touchPosition = touch.position;

                // Assuming the screen is split in half, left and right
                if (touchPosition.x < Screen.width / 2)
                {
                    move = -1; // Move left
                }
                else if (touchPosition.x > Screen.width / 2)
                {
                    move = 1; // Move right
                }
            }
        }


    }

    private void FixedUpdate()
    {
        transform.RotateAround(Vector3.zero, Vector3.forward,-move*Time.fixedDeltaTime*speed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("PlayerCollided");

        SceneManager.LoadScene("HexaLevel");
    }
}
